<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/clear', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cleared!";
});
Auth::routes(['register'=>false]);

Route::get('/', 'HomeController@welcome')->name('welcome');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/product/{id}', 'HomeController@product')->name('product');
Route::get('/our/product', 'HomeController@ourProduct')->name('our.product');
Route::get('/about/us', 'HomeController@aboutUs')->name('about.us');
Route::get('/contact/us', 'HomeController@contactUs')->name('contact.us');

Route::group(['as'=>'admin.','prefix'=>'admin','namespace'=>'Admin','middleware'=>['auth','admin','history']], function () {
    Route::get('/dashboard','DashboardController@index')->name('dashboard');

    Route::get('/logo/name','DashboardController@logoName')->name('logo.name');
    Route::put('/logo/name/update','DashboardController@logoNameUpdate')->name('logo.name.update');

    Route::get('/social','DashboardController@social')->name('social');
    Route::put('/social/update','DashboardController@socialUpdate')->name('social.update');

    Route::get('/contact','DashboardController@contact')->name('contact');
    Route::put('/contact/update','DashboardController@contactUpdate')->name('contact.update');

    Route::get('/about/us','DashboardController@aboutUs')->name('about.us');
    Route::put('/about/us/update','DashboardController@aboutUsUpdate')->name('about-us.update');

    Route::get('/category/add','CategoryController@category')->name('category');
    Route::post('/category/add/add','CategoryController@categoryAdd')->name('category.add');
    Route::get('/category/view','CategoryController@categoryView')->name('category.view');
    Route::get('/category/edit{id}','CategoryController@categoryEdit')->name('category.edit');
    Route::put('/category/update{id}','CategoryController@categoryUpdate')->name('category.update');
    Route::delete('/category/delete{id}','CategoryController@categoryDelete')->name('category.delete');

    Route::get('/product/add','ProductController@product')->name('product');
    Route::post('/product/add/add','ProductController@productAdd')->name('product.add');
    Route::get('/product/{id}edit','ProductController@productedit')->name('product.edit');
    Route::get('/product/view','ProductController@productView')->name('product.view');
    Route::put('/product{id}/update','ProductController@productUpdate')->name('product.update');
    Route::delete('/product/delete{id}','ProductController@productDelete')->name('product.delete');

});

