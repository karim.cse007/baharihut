<?php

namespace App\Http\Controllers;

use App\AboutUs;
use App\Category;
use App\ContactInfo;
use App\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
    public function welcome(){
        $products = Product::all();
        return view('welcome',compact('products'));
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function product($id){
        $product = Product::findOrFail($id);
        if(isset($product->id)){
            return view('singleProduct',compact('product'));
        }
        return redirect()->back();
    }
    public function ourProduct(){
        $categories = Category::all();
        return view('our_product',compact('categories'));
    }
    public function aboutUs(){
        $about = AboutUs::first();
        return view('about_us',compact('about'));
    }
    public function contactUs(){
        $contact = ContactInfo::first();
        return view('contact_us',compact('contact'));
    }
}
