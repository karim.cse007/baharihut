<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    public function product(){
        //dd('Bahari'.str_pad(mt_rand(100,999),3,3,STR_PAD_LEFT));
        $categories = Category::all();
        return view('Admin.Product.create',compact('categories'));
    }
    public function productAdd(Request $request, Product $product){
        $val = Validator::make($request->all(),[
            'name' => 'required|string|max:191',
            'price'=>'required|integer|not_in:0',
            'description'=>'required|string',
            'condition'=>'required|string',
            'category'=>'required|integer|not_in:0|exists:categories,id',
            'image1'=>'required|image|mimes:jpg,jpeg,png,gif',
            'image2'=>'required|image|mimes:jpg,jpeg,png,gif',
        ]);
        if ($val->fails()){
            Toastr::error('Invalid Request','Error');
            return redirect()->withInput();
        }

        //get from image
        $image1 = $request->file('image1');
        $image2 = $request->file('image2');
        if (isset($image1) && isset($image2)){
            $img1 = Str::slug($request->name).'-'.uniqid().'.'.$image1->getClientOriginalExtension();
            $img2 = Str::slug($request->name).'-'.uniqid().'.'.$image2->getClientOriginalExtension();
            //check category dir is exits
            if (!Storage::disk('public')->exists('product')){
                Storage::disk('public')->makeDirectory('product');
            }
            $pImg1 = Image::make($image1)->resize(400,300)->save();
            Storage::disk('public')->put('product/'.$img1,$pImg1);
            $pImg2 = Image::make($image2)->resize(400,300)->save();
            Storage::disk('public')->put('product/'.$img2,$pImg2);

            $product->name = $request->name;//str_pad(mt_rand(0, 999999), 6, '0', STR_PAD_LEFT)
            $product->code = 'Bahari'.str_pad(mt_rand(100,999),3,3,STR_PAD_LEFT);
            $product->category_id=$request->category;
            $product->price = $request->price;
            $product->description = $request->description;
            $product->condition = $request->condition;
            $product->image1 = $img1;
            $product->image2 = $img2;
            $product->save();
            Toastr::success('successfully added :)','Success');
            return redirect()->back();
        }
        Toastr::error('Invalid Request','Error');
        return redirect()->back();

    }
    public function productView(){
        $products = Product::all();
        return view('Admin.Product.index',compact('products'));
    }
    public function productEdit($id){
        $product = Product::findOrFail($id);
        if (isset($product->id)){
            $categories = Category::all();
            return view('Admin.Product.edit',compact('product','categories'));
        }
        Toastr::error('Invalid Request','Error');
        return redirect()->back();
    }
    public function productUpdate(Request $request,$id){

        $val = Validator::make($request->all(),[
            'name' => 'required|string|max:191',
            'price'=>'required|integer|not_in:0',
            'description'=>'required|string',
            'condition'=>'required|string',
            'category'=>'required|integer|not_in:0|exists:categories,id',
            'image1'=>'image|mimes:jpg,jpeg,png,gif',
            'image2'=>'image|mimes:jpg,jpeg,png,gif',
        ]);
        if ($val->fails()){
            Toastr::error('Invalid Request','Error');
            return redirect()->withInput();
        }
        $product = Product::findOrFail($id);
        if (isset($product->id)){
            //get from image
            $image1 = $request->file('image1');
            $image2 = $request->file('image2');
            if (isset($image1) && isset($image2)){
                $img1 = Str::slug($request->name).'-'.uniqid().'.'.$image1->getClientOriginalExtension();
                $img2 = Str::slug($request->name).'-'.uniqid().'.'.$image2->getClientOriginalExtension();
                //check category dir is exits
                if (!Storage::disk('public')->exists('product')){
                    Storage::disk('public')->makeDirectory('product');
                }
                //remove previous image
                if (Storage::disk('public')->exists('product/'.$product->image1)){
                    Storage::disk('public')->delete('product/'.$product->image1);
                }
                if (Storage::disk('public')->exists('product/'.$product->image2)){
                    Storage::disk('public')->delete('product/'.$product->image2);
                }

                $pImg1 = Image::make($image1)->resize(400,300)->save();
                Storage::disk('public')->put('product/'.$img1,$pImg1);
                $pImg2 = Image::make($image2)->resize(400,300)->save();
                Storage::disk('public')->put('product/'.$img2,$pImg2);
            }else{
                $img1=$product->image1;
                $img2=$product->image2;
            }
            $product->name = $request->name;
            $product->category_id=$request->category;
            $product->price = $request->price;
            $product->description = $request->description;
            $product->condition = $request->condition;
            $product->image1 = $img1;
            $product->image2 = $img2;
            $product->save();
            Toastr::success('successfully update :)','Success');
            return redirect()->back();
        }
        Toastr::error('Invalid Request','Error');
        return redirect()->back();
    }
    public function productDelete($id){
        $pod = Product::findOrFail($id);
        if (isset($pod->id)){
            if (Storage::disk('public')->exists('product/'.$pod->image1)){
                Storage::disk('public')->delete('product/'.$pod->image1);
            }
            if (Storage::disk('public')->exists('product/'.$pod->image2)){
                Storage::disk('public')->delete('product/'.$pod->image2);
            }
            $pod->delete();
            Toastr::success('Delete successfully','Success');
            return redirect()->back();
        }
        Toastr::error('Invalid Request','Error');
        return redirect()->back();
    }
}
