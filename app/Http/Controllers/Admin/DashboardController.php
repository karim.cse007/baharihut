<?php

namespace App\Http\Controllers\Admin;

use App\AboutUs;
use App\ContactInfo;
use App\Http\Controllers\Controller;
use App\LogoName;
use App\Social;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class DashboardController extends Controller
{
    public function index(){
        return view('Admin.dashboard');
    }
    public function logoName(){
        $info = LogoName::first();
        return view('Admin.logo_name',compact('info'));
    }
    public function logoNameUpdate(Request $request){
        $val = Validator::make($request->all(),[
            'name'=>'required|string|max:191',
            'logo'=>'image|mimes:jpg,jpeg,png,gif',
        ]);
        if ($val->fails()){
            Toastr::error('Invalid Request','Error');
            return redirect()->back();
        }
        $logoName = LogoName::first();
        $logo = $request->file('logo');
        if (isset($logoName->id)){
            if (isset($logo)){
                $logoN = 'logo'.'.'.$logo->getClientOriginalExtension();
                if(!Storage::disk('public')->exists('logo')){
                    Storage::disk('public')->makeDirectory('logo');
                }
                $img = Image::make($logo)->resize(200,100)->save();
                Storage::disk('public')->put('logo/'.$logoN,$img);
            }else{
                $logoN=$logo->logo;
            }
            $logoName->name=$request->name;
            $logo->logo=$logoN;
            $logo->save();
            Toastr::success('Successfully update','Success');
            return redirect()->back();
        }else{
            if (!isset($logo)){
                Toastr::error('please select logo first','Error');
                return redirect()->back()->withInput();
            }
            $logoN = 'logo'.'.'.$logo->getClientOriginalExtension();
            if(!Storage::disk('public')->exists('logo')){
                Storage::disk('public')->makeDirectory('logo');
            }
            $img = Image::make($logo)->resize(200,100)->save();
            Storage::disk('public')->put('logo/'.$logoN,$img);
            $log = new LogoName();
            $log->name=$request->name;
            $log->logo = $logoN;
            $log->save();
            Toastr::success('Successfully set logo and name','Success');
            return redirect()->back();
        }
    }
    public function social(){
        $socials = Social::first();
        return view('Admin.social',compact('socials'));
    }
    public function socialUpdate(Request $requests){
        $val = Validator::make($requests->all(),[
            'facebook'=>'string|max:191',
            'twitter'=>'string|max:191',
            'instagram'=>'string|max:191',
            'youtube'=>'string|max:191',
            'linkedin'=>'string|max:191',
            'skype'=>'string|max:191',
        ]);
        if ($val->fails()){
            Toastr::error('Invalid Request','Success');
            return redirect()->back();
        }
        $social = Social::first();
        if (!isset($social->id)){
            $social = new Social();
        }
        $social->fb = $requests->facebook;
        $social->tw = $requests->twitter;
        $social->instagram = $requests->instagram;
        $social->youtube = $requests->youtube;
        $social->linkedin = $requests->linkedin;
        $social->skype = $requests->skype;
        $social->save();
        Toastr::success('Successfully Update','Success');
        return redirect()->back();
    }
    public function contact(){
        $info = ContactInfo::first();
        return view('Admin.contact_info',compact('info'));
    }
    public function contactUpdate(Request $request){
        $val = Validator::make($request->all(),[
            'email'=>'required|email|string|max:191',
            'phone1'=>'required|string|max:191',
            'phone2'=>'required|string|max:191',
            'address'=>'required|string',
            'map'=>'required|string',
        ]);
        if ($val->fails()){
            Toastr::error('Invalid Request','Success');
            return redirect()->back();
        }
        $contact= ContactInfo::first();
        if (!isset($social->id)){
            $contact = new ContactInfo();
        }
        $contact->phone1 = $request->phone1;
        $contact->phone2 = $request->phone2;
        $contact->email = $request->email;
        $contact->address = $request->address;
        $contact->map_location = $request->map;
        $contact->save();
        Toastr::success('Successfully Update','Success');
        return redirect()->back();
    }
    public function aboutUs(){
        $info = AboutUs::first();
        return view('Admin.about_us',compact('info'));
    }
    public function aboutUsUpdate(Request $request){
        $val=Validator::make($request->all(),[
           'title' =>'required|string|max:191',
            'description'=>'required|string',
            'image'=>'image|mimes:jpg,jpeg,png,gif',
        ]);
        if ($val->fails()){
            Toastr::error('Invalid Request','Error');
            return redirect()->back();
        }
        $about = AboutUs::first();
        $image = $request->file('image');
        if (isset($about->id)){
            if (isset($image)){
                $imgName = 'about-us'.'.'.$image->getClientOriginalExtension();
                $img = Image::make($image)->resize(400,500)->save();
                Storage::disk('public')->put($imgName,$img);
            }else{
                $imgName= $about->image;
            }
        }else{
            if (isset($image)){
                $imgName = 'about-us'.'.'.$image->getClientOriginalExtension();
                $img = Image::make($image)->resize(400,500)->save();
                Storage::disk('public')->put($imgName,$img);
            }else{
                Toastr::error('Please select image first','Error');
                return redirect()->back();
            }
            $about = new AboutUs();
        }
        $about->title = $request->title;
        $about->image = $imgName;
        $about->description = $request->description;
        $about->save();
        Toastr::success('Successfully updated','Success');
        return redirect()->back();
    }
}
