<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function category(){
        return view('Admin.add_category');
    }
    public function categoryAdd(Request $request){
        $val = Validator::make($request->all(),[
            'name'=>'required|string|max:191|unique:categories,name',
        ]);
        if ($val->fails()){
            Toastr::error('Invalid Request!!','Error');
            return redirect()->back();
        }
        $category = new Category();
        $category->name = $request->name;
        $category->save();
        Toastr::success('Successfully Added','Success');
        return redirect()->back();
    }
    public function categoryView(){
        $categories = Category::all();
        return view('Admin.Category.index',compact('categories'));
    }
    public function categoryEdit($id){
        $cat = Category::findOrFail($id);
        if (isset($cat->id)){
            return view('Admin.Category.edit',compact('cat'));
        }
        Toastr::error('Invalid Request','Error');
        return redirect()->back();
    }
    public function categoryUpdate(Request $request, $id){
        //dd($id);
        $val = Validator::make($request->all(),[
            'name'=>'required|string|max:191',
        ]);
        if ($val->fails()){
            Toastr::error('Invalid Request!!','Error');
            return redirect()->back();
        }
        $category=Category::findOrFail($id);
        $category->name = $request->name;
        $category->save();
        Toastr::success('Successfully Update','Success');
        return redirect()->back();
    }
    public function categoryDelete($id){
        $category = Category::findOrFail($id);
        if (isset($category->id)){
            if($category->products->count() >0){
                Toastr::error('This category have product. you cant delete this category.','Error');
                return redirect()->back();
            }
            $category->delete();
            Toastr::success('Successfully delete','Success');
            return redirect()->back();
        }
        Toastr::error('Invalid request','Error');
        return redirect()->back();
    }
}
