@extends('layouts.frontend.app')

@section('title','Product')

@push('css')

@endpush
@section('content')
    <div class="breadcrumb parallax-container">
        <h1>Product</h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="content col-sm-12">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="thumbnails">
                            <div>
                                <a class="thumbnail fancybox" href="{{asset('public/storage/product/'.$product->image1)}}"
                                    title="Casual Shirt With Ruffle Hem">
                                    <img src="{{asset('public/storage/product/'.$product->image1)}}" title="{{$product->name}}"
                                         alt="{{$product->name}}"/></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-7 prodetail">
                        <h1 class="productpage-title">{{$product->name}}</h1>
                        <div class="rating"> <span class="fa fa-stack">
                                <i class="fa fa-star-o fa-stack-2x"></i>
                                <i class="fa fa-star fa-stack-2x"></i>
                            </span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i>
                                <i class="fa fa-star fa-stack-2x"></i></span> <span class="fa fa-stack">
                                <i class="fa fa-star-o fa-stack-2x"></i><i class="fa fa-star fa-stack-2x"></i>
                            </span> <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i>
                                <i class="fa fa-star fa-stack-2x"></i></span> <span class="fa fa-stack">
                                <i class="fa fa-star-o fa-stack-2x"></i></span><span class="riview">
                                <a href="javascript:void(0)">reviews</a> / <a href="javascript:void(0)">Write a review</a></span></div>
                        <ul class="list-unstyled productinfo-details-top">
                            <li>
                                <h2 class="productpage-price">{{$product->price}} Tk./Kg</h2>
                            </li>
                        </ul>
                        <hr>
                        <ul class="list-unstyled product_info">
                            <li>
                                <label>Product Code:</label>
                                <span> {{$product->code}}</span></li>
                            <li>
                                <label>Availability:</label>
                                <span> {{$product->is_available==true?'In stock':'Out of Stock'}}</span></li>
                        </ul>
                        <hr>
                        <p class="product-desc"></p>
                        <div id="product">
                            <div class="form-group">
                                <div class="qty">
                                    <!----------
                                    <label>Weight(Kg)</label>
                                    <input id="qty" placeholder="1" type="number">
                                    -------->
                                    <ul class="button-group list-btn">
                                        <li>
                                            <button type="button" class="addtocart-btn" data-toggle="tooltip"
                                                    data-placement="top" title="Order Now">
                                                <a href="https://forms.gle/bg4EtSXYttV5Fhbp6" target="_blank">
                                                    <i class="fa fa-shopping-bag"></i>
                                                </a>
                                            </button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="productinfo-tab">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-description" data-toggle="tab">Description</a></li>
                        <li><a href="#tab-condition" data-toggle="tab">Condition</a></li>
                        <li><a href="#tab-review" data-toggle="tab">Reviews (1)</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-description">
                            <div class="cpt_product_description ">
                                <div>
                                    {!! $product->description !!}
                                </div>
                            </div>
                            <!-- cpt_container_end --></div>
                        <div class="tab-pane" id="tab-condition">
                            <div class="cpt_product_description ">
                                <div>
                                    {!! $product->condition !!}
                                </div>
                            </div>
                            <!-- cpt_container_end --></div>
                        <div class="tab-pane" id="tab-review">
                            <form class="form-horizontal">
                                <div id="review"></div>
                                <h2>Write a review</h2>
                                <div class="form-group required">
                                    <div class="col-sm-12">
                                        <label class="control-label" for="input-name">Your Name</label>
                                        <input type="text" name="name" value="" id="input-name" class="form-control"/>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <div class="col-sm-12">
                                        <label class="control-label" for="input-review">Your Review</label>
                                        <textarea name="text" rows="5" id="input-review"
                                                  class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <div class="col-sm-12">
                                        <label class="control-label">Rating</label>
                                        &nbsp;&nbsp;&nbsp; Bad&nbsp;
                                        <input type="radio" name="rating" value="1"/>
                                        &nbsp;
                                        <input type="radio" name="rating" value="2"/>
                                        &nbsp;
                                        <input type="radio" name="rating" value="3"/>
                                        &nbsp;
                                        <input type="radio" name="rating" value="4"/>
                                        &nbsp;
                                        <input type="radio" name="rating" value="5"/>
                                        &nbsp;Good
                                    </div>
                                </div>
                                <div class="buttons clearfix">
                                    <div class="pull-right">
                                        <button type="submit" id="button-review" data-loading-text="Loading..."
                                                class="btn btn-primary">Submit
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <h3 class="productblock-title">Related Products</h3>
                <div class="box">
                    <div id="related-slidertab" class="row owl-carousel product-slider">
                        @foreach($product->category->products as $pod)
                            <div class="item">
                                <div class="product-thumb transition">
                                    <div class="image product-imageblock"><a href="{{route('product',$pod->id)}}">
                                            <img src="{{asset('public/storage/product/'.$pod->image1)}}" alt="{{$product->name}}" title="{{$product->name}}"
                                                 class="img-responsive"/>
                                            <img src="{{asset('public/storage/product/'.$pod->image2)}}" alt="{{$product->name}}" title="{{$product->name}}"
                                                 class="img-responsive"/>
                                        </a>
                                        <ul class="button-group">
                                            <li>
                                                <button title="Add to Cart" class="addtocart-btn" type="button">
                                                    <a href="https://forms.gle/bg4EtSXYttV5Fhbp6" target="_blank">
                                                        Order Now
                                                    </a>
                                                </button>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="caption product-detail">
                                        <h4 class="product-name">
                                            <a href="{{route('product',$pod->id)}}" title="Casual Shirt With Ruffle Hem">{{$pod->name}}</a></h4>
                                        <p class="price product-price">{{$pod->price}} Tk./Kg
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')

@endpush
