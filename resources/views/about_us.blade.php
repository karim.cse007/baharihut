@extends('layouts.frontend.app')

@section('title','About us')

@push('css')

@endpush
@section('content')
    <div class="breadcrumb parallax-container">
        <h1>{{isset($about->id)?$about->title:''}} </h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="wwd">
                <div class="col-sm-5">
                    <img class="img-responsive" src="{{isset($about->id)?asset('public/storage/'.$about->image):''}}" alt="{{isset($about->id)?$about->title:''}}"></div>
                <div class="col-sm-7">
                    <div class="column-inner ">
                        <div class="wrapper">
                            <div class="desc">
                                {{isset($about->id)?$about->description:''}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')

@endpush
