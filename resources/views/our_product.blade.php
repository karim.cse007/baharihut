@extends('layouts.frontend.app')

@section('title','Home')

@push('css')

@endpush
@section('content')
    <div id="center">
        <div class="container">
            <div class="row">
                <div class="content col-sm-12">
                    @foreach ($categories as $category)
                        @if ($category->products->count() >=1 )
                            <div class="customtab">
                                <h3 class="productblock-title">{{$category->name}}</h3>
                                <div id="tabs" class="customtab-wrapper">
                                    <ul class='customtab-inner'>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="product-layout  product-grid  col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                    @foreach($category->products as $product)
                                        <div class="item">
                                        <div class="product-thumb">
                                            <div class="image product-imageblock">
                                                <a href="javascript:void(0)">
                                                    <img src="{{asset('public/storage/product/'.$product->image1)}}" alt="{{$product->name}}" title="{{$product->name}}" class="img-responsive" />
                                                    <img src="{{asset('public/storage/product/'.$product->image2)}}" alt="{{$product->name}}" title="{{$product->name}}" class="img-responsive" />
                                                </a>
                                                <ul class="button-group">
                                                    <li>
                                                        <button type="button" class="addtocart-btn" title="Order">
                                                            <a href="https://forms.gle/bg4EtSXYttV5Fhbp6" target="_blank"> Order Now</a></button>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="caption product-detail">
                                                <h4 class="product-name">
                                                    <a href="#" title="{{$product->name}}">{{$product->name}}</a></h4>
                                                <p class="price product-price">{{$product->price}}Tk./Kg<span class="price-tax">{{$product->price}} Tk./Kg</span></p>
                                                <button type="button" class="btn btn-primary">
                                                    <a href="{{route('product',$product->id)}}">Bistarito</a>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')

@endpush
