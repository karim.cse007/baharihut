@extends('layouts.frontend.app')

@section('title','Home')

@push('css')

@endpush
@section('content')
    <div class="breadcrumb parallax-container">
        <h1>Contact Us</h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <h3 class="contactus-title">You Have Got Questions We have Got Answers</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="complaint">
                    <h2 class="tf">Tel</h2>
                    <div class="call-info">{{isset($contact->id)?$contact->phone1:''}}</div>
                    <div class="call-info">{{isset($contact->id)?$contact->phone2:''}}</div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="email">
                    <h2 class="tf">Mail</h2>
                    <div class="email-info">{{isset($contact->id)?$contact->email:''}}</div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="time">
                    <h2 class="tf">Time</h2>
                    <div class="time-info">Sat – Fri: 9:00AM – 8PM</div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="time">
                    <h2 class="tf">Address</h2>
                    <div class="home">
                        {{$contact->address}}
                    </div>
                </div>
            </div>
        </div><br>
        <div class="row">
            <div class="col-sm-12">
                <div class="map" id="map">
                    {!!  isset($contact->id)?$contact->map_location:''!!}
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')

@endpush
