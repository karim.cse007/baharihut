<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            <img src="{{ asset('public/storage/admin/'.Auth::user()->image) }}" width="48" height="48" alt="User" />
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</div>
            <div class="email">{{ Auth::user()->email }}</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">

                    <li>
                        <a href="">
                            <i class="material-icons">settings</i>Settings</a>
                    </li>
                    <li role="separator" class="divider"></li>
                    <li>
                        <a class="dropdown-item" href="javascript:void(0);"
                           onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                            <i class="material-icons">input</i>Sign Out
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>

                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>

            @if(Request::is('admin*'))
                <li class="{{ Request::is('admin/dashboard*') ? 'active' : '' }}">
                    <a href="{{route('admin.dashboard')}}">
                        <i class="material-icons">dashboard</i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/logo/name*') ? 'active' : '' }}">
                    <a href="{{route('admin.logo.name')}}">
                        <i class="material-icons">alarm_add</i>
                        <span>Logo name</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/social*') ? 'active' : '' }}">
                    <a href="{{route('admin.social')}}">
                        <i class="material-icons">alarm_add</i>
                        <span>Social links</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle {{Request::is('admin/category*')?'toggled':''}}">
                        <i class="material-icons">accessibility</i>
                        <span>Category</span>
                    </a>
                    <ul class="ml-menu">
                        <li class="{{Request::is('admin/category/add')?'active':''}}">
                            <a href="{{route('admin.category')}}">Add Category</a>
                        </li>
                        <li class="{{ Request::is('admin/category/view') ? 'active' : '' }}">
                            <a href="{{route('admin.category.view')}}">Manage Category</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0);" class="menu-toggle {{Request::is('admin/product*')?'toggled':''}}">
                        <i class="material-icons">accessibility</i>
                        <span>Product</span>
                    </a>
                    <ul class="ml-menu">
                        <li class="{{Request::is('admin/product/add')?'active':''}}">
                            <a href="{{route('admin.product')}}">Add Product</a>
                        </li>
                        <li class="{{ Request::is('admin/product/view') ? 'active' : '' }}">
                            <a href="{{route('admin.product.view')}}">Manage Product</a>
                        </li>
                    </ul>
                </li>

                <li class="{{ Request::is('admin/about/us*') ? 'active' : '' }}">
                    <a href="{{route('admin.about.us')}}">
                        <i class="material-icons">info</i>
                        <span>About Us</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/contact*') ? 'active' : '' }}">
                    <a href="{{route('admin.contact')}}">
                        <i class="material-icons">perm_contact_calendar</i>
                        <span>Contact Information</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/seo') ? 'active' : '' }}">
                    <a href="#">
                        <i class="material-icons">perm_contact_calendar</i>
                        <span>SEO</span>
                    </a>
                </li>
                <li class="header">System</li>
                <li class="{{ Request::is('admin/settings') ? 'active' : '' }}">
                    <a href="#">
                        <i class="material-icons">settings</i>
                        <span>Settings</span>
                    </a>
                </li>
                <li>
                    <a class="dropdown-item" href="javascript:void(0);"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <i class="material-icons">input</i>
                        <span>Log out</span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            @endif

        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            &copy; {{date('Y')}} <a href="javascript:void(0);">Admin</a>.
        </div>
        <div class="version">
            <b>Version: </b> 1.0.5
        </div>
    </div>
    <!-- #Footer -->
</aside>
<!-- #END# Left Sidebar -->

