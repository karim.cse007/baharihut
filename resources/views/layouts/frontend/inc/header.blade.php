<header>
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="top-left pull-left">
                        <div class="language">
                            <form action="Baharihut" method="post" enctype="multipart/form-data" id="language">
                                <div class="btn-group">
                                    <button class="btn btn-link dropdown-toggle" data-toggle="dropdown"
                                            aria-expanded="false"> Bangla <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:void(0)">Bangla</a></li>
                                        <li><a href="javascript:void(0)"> English</a></li>
                                    </ul>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="wel-come-msg"> Welcome to Baharihut!</div>
                </div>
                <div class="top-right pull-right">
                    <div id="top-links" class="nav pull-right">
                        <ul class="list-inline">
                            <li class="dropdown">
                                <a href="javascript:void(0)" title="My Account" class="dropdown-toggle"
                                                    data-toggle="dropdown">
                                    <i class="fa fa-user" aria-hidden="true"></i><span>My Account</span>
                                    <span class="caret"></span></a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    @guest
                                        @if (Route::has('register'))
                                            <li><a href="{{route('register')}}">Register</a></li>
                                        @endif
                                        <li><a href="{{route('login')}}">Login</a></li>
                                    @else
                                        <li>
                                            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
                                        </li>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    @endguest
                                </ul>
                            </li>
                            <li><a href="javascript:void(0)" id="wishlist-total" title="Wish List (0)"><i
                                        class="fa fa-heart"
                                        aria-hidden="true"></i><span>Wish List</span><span> (0)</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="header-inner">
            <div class="col-sm-3 col-xs-3 header-left">
                <div id="logo">
                    <a href="/">
                        <img src="{{isset(\App\LogoName::first()->logo)?asset('public/storage/logo/'.\App\LogoName::first()->logo):'' }}" title="Baharihut" alt="Baharihut"
                             class="img-responsive"/></a>
                </div>
            </div>
            <div class="col-sm-9 col-xs-9 header-right">
                <div id="search" class="input-group">
                    <input type="text" name="search" value="" placeholder="Enter your keyword ..."
                           class="form-control input-lg"/>
                    <span class="input-group-btn">
          <button type="button" class="btn btn-default btn-lg"><span>Search</span></button>
          </span></div>
                <div id="cart" class="btn-group btn-block">
                    <button type="button" class="btn btn-inverse btn-block btn-lg dropdown-toggle cart-dropdown-button"> <span
                            id="cart-total"><span>Shopping Cart</span><br>
                                0 item(s) - 0 Tk.</span>
                    </button>
                    <ul class="dropdown-menu pull-right cart-dropdown-menu">
                        <li>
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <td class="text-center">
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<nav id="menu" class="navbar">
    <div class="nav-inner">
        <div class="navbar-header"><span id="category" class="visible-xs">Categories</span>
            <button type="button" class="btn btn-navbar navbar-toggle"><i class="fa fa-bars"></i></button>
        </div>
        <div class="navbar-collapse">
            <ul class="main-navigation">
                <li><a href="{{route('welcome')}}" class="parent">Home</a></li>
                <li><a href="{{route('our.product')}}" class="active">Our Product</a></li>
                <li><a href="{{route('about.us')}}">About us</a></li>
                <li><a href="{{route('contact.us')}}">Contact Us</a></li>
            </ul>
        </div>
    </div>
</nav>
