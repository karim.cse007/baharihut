<footer>
    <div class="cms_searvice">
        <div class="container">
            <div class="row">
                <div class="col-md-3 ">
                    <div class="cms-block1 z-depth-5">
                        <h4>Easy Shipping</h4>
                        <p>To your door</p>
                    </div>
                </div>
                <div class="col-md-3 ">
                    <div class="cms-block2">
                        <h4>Easy Payment</h4>
                        <p></p>
                    </div>
                </div>
                <div class="col-md-3 ">
                    <div class="cms-block3">
                        <h4>24/7 Support</h4>
                        <p>Feel free to Contact us</p>
                    </div>
                </div>
                <div class="col-md-3 ">
                    <div class="cms-block4">
                        <h4>Online Shopping </h4>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div id="bottom-footer">
            <div class="footer-bottom-cms">
                <div class="footer-payment">
                    <ul>
                        <li class="mastero"><a href="#"><img alt="" src="{{asset('assets/frontend/image/payment/mastero.jpg')}}"></a></li>
                        <li class="visa"><a href="#"><img alt="" src="{{asset('assets/frontend/image/payment/visa.jpg')}}"></a></li>
                        <li class="currus"><a href="#"><img alt="" src="{{asset('assets/frontend/image/payment/currus.jpg')}}"></a></li>
                        <li class="discover"><a href="#"><img alt="" src="{{asset('assets/frontend/image/payment/discover.jpg')}}"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <a id="scrollup">Scroll</a> </footer>
