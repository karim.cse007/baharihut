<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title') -{{ config('', 'Baharihut') }}</title>

    <!-- Favicon Icon -->
    <link rel="shortcut icon" href="{{asset('assets/favico.png')}}">
    <link rel="apple-touch-icon" href="{{asset('assets/favico.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('assets/favico.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('assets/favico.png')}}">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/frontend/css/bootstrap.min.css')}}" rel="stylesheet" media="screen" />
    <link href="{{asset('assets/frontend/javascript/font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/frontend/css/stylesheet.css')}}" rel="stylesheet">
    <link href="{{asset('assets/frontend/css/responsive.css')}}" rel="stylesheet">
    <link href="{{asset('assets/frontend/javascript/owl-carousel/owl.carousel.css')}}" type="text/css" rel="stylesheet" media="screen" />
    <link href="{{asset('assets/frontend/javascript/owl-carousel/owl.transitions.css')}}" type="text/css" rel="stylesheet" media="screen" />
    <script type="text/javascript" src="{{asset('assets/frontend/javascript/jquery-2.1.1.min.js')}}" ></script>
    <script type="text/javascript" src="{{asset('assets/frontend/javascript/bootstrap/js/bootstrap.min.js')}}" ></script>
    <script type="text/javascript" src="{{asset('assets/frontend/javascript/template_js/jstree.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/frontend/javascript/template_js/template.js')}}" ></script>
    <script type="text/javascript" src="{{asset('assets/frontend/javascript/common.js')}}" ></script>
    <script type="text/javascript" src="{{asset('assets/frontend/javascript/global.js')}}" ></script>
    <script type="text/javascript" src="{{asset('assets/frontend/javascript/owl-carousel/owl.carousel.min.js')}}" ></script>

    <!-- Waves Effect Css -->
    <link rel="stylesheet" href="https://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">

    @stack('css')

</head>
<body class="index">
<div class="preloader loader" style="display: block;"> <img src="{{asset('assets/frontend/image/loader.gif')}}"  alt="Baharihut"/></div>
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->
@include('layouts.frontend.inc.header')
<section class="content">
    @yield('content')
</section>
@include('layouts.frontend.inc.footer')
<!-- Bootstrap Core Js -->
<script src="{{asset('assets/frontend/javascript/jquery.parallax.js')}}"></script>
<script>
    jQuery(document).ready(function ($) {
        $('.parallax').parallax();
    });
</script>

@stack('js')
</body>
</html>
