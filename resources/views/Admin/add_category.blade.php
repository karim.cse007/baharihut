@extends('layouts.backend.app')

@section('title','Add Category')

@push('css')

@endpush
@section('content')

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Add New Category
                        </h2>
                    </div>
                    <div class="body">
                        <h2 class="card-inside-title">Category</h2>
                        <form action="{{route('admin.category.add')}}" method="post">
                            @csrf
                            @method('POST')
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" name="name" class="form-control" placeholder="Name" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div>
                                            <input type="submit" value="Add" class="btn btn-primary" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Input -->
    </div>

@endsection

@push('js')

@endpush
