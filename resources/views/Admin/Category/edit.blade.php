@extends('layouts.backend.app')

@section('title','Edit')

@push('css')
    <!-- Bootstrap Select Css -->
    <link href="{{ asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endpush

@section('content')
    <div class="container-fluid">

        <!-- Vertical Layout | With Floating Label -->
        <div class="row clearfix">
            <form action="{{ route('admin.category.update',$cat->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Update Category
                            </h2>
                        </div>
                        <div class="body">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="title" class="form-control" name="name" value="{{ $cat->name }}">
                                    <label class="form-label">Name</label>
                                </div>
                            </div>
                            <div class="card">
                                <div class="header">
                                </div>
                                <a class="btn btn-danger m-t-15 waves-effect" href="{{ route('admin.category.view') }}">Back</a>
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">Update</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- Vertical Layout | With Floating Label -->
    </div>

@endsection

@push('js')
    <!-- Select Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
@endpush
