@extends('layouts.backend.app')

@section('title','Contact-information')

@push('css')

@endpush
@section('content')
    <div class="container-fluid">
        <!-- Vertical Layout -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            CONTACT INFORMATION
                        </h2>
                    </div>
                    <div class="body">
                        <form action="{{route('admin.contact.update')}}" method="post">
                            @csrf
                            @method('PUT')
                            <label for="email_address">Phone number 1</label>
                            <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">phone</i>
                                            </span>
                                <div class="form-line">
                                    <input type="text" name="phone1" value="{{ isset($info->phone1)?$info->phone1:'' }}" class="form-control mobile-phone-number" placeholder="Ex: +880xxxxxxxxxx">
                                </div>
                            </div>
                            <label for="email_address">Phone number 2</label>
                            <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">phone</i>
                                            </span>
                                <div class="form-line">
                                    <input type="text" name="phone2" value="{{ isset($info->phone2)?$info->phone2:'' }}" class="form-control mobile-phone-number" placeholder="Ex: +880xxxxxxxxxx">
                                </div>
                            </div>
                            <label for="email_address">Email Address</label>
                            <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">email</i>
                                            </span>
                                <div class="form-line">
                                    <input type="text" name="email" value="{{ isset($info->email)?$info->email:'' }}" class="form-control email" placeholder="Ex: example@example.com">
                                </div>
                            </div>
                            <label for="email_address">Location</label>
                            <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">location_city</i>
                                            </span>
                                <div class="form-line">
                                    <input type="text" name="address" value="{{ isset($info->address)?$info->address:'' }}" class="form-control mobile-phone-number">
                                </div>
                            </div>
                            <label for="email_address">Maps</label>
                            <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">near_me</i>
                                            </span>
                                <div class="form-line">
                                    <input type="text" name="map" value="{{ isset($info->map_location)?$info->map_location:'' }}" class="form-control mobile-phone-number">
                                </div>
                            </div>


                            <br>
                            <input type="submit" class="btn btn-primary" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Vertical Layout -->

    </div>
@endsection

@push('js')

@endpush
