@extends('layouts.backend.app')

@section('title','About Us')

@push('css')

@endpush
@section('content')

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Update About Us
                        </h2>
                    </div>
                    <div class="body">
                        <form action="{{route('admin.about-us.update')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="body">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" id="title" value="{{isset($info->title)?$info->title:''}}" class="form-control" name="title">
                                        <label class="form-label">Title</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <img src="{{isset($info->image)?asset('public/storage/'.$info->image):''}}">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Upload Images</label>
                                    <input type="file" name="image">
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Description</label>
                                    <textarea class="form-control" id="description" rows="6" name="description" style="outline: none !important;border-color: #719ECE;box-shadow: 0 0 10px #719ECE;"> {!! isset($info->description)?$info->description:'' !!}</textarea>
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary" value="Submit">
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Input -->
    </div>

@endsection

@push('js')

@endpush
