@extends('layouts.backend.app')

@section('title','Create')

@push('css')
    <!-- Bootstrap Select Css -->
    <link href="{{ asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endpush


@section('content')
    <div class="container-fluid">

        <!-- Vertical Layout | With Floating Label -->
        <div class="row clearfix">
            <form action="{{ route('admin.product.update',$product->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Update Product
                            </h2>
                        </div>
                        <div class="body">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="name" value="{{$product->name}}" class="form-control" name="name">
                                    <label class="form-label">Food Name</label>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="float" id="price" value="{{$product->price}}" class="form-control" name="price">
                                    <label class="form-label">Price per Kg</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Upload Images 1</label>
                                <input type="file" name="image1">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Upload Images 2</label>
                                <input type="file" name="image2">
                            </div>
                            <div class="form-group">
                                <label class="form-label">Description</label>
                                <textarea class="form-control" id="description" rows="5" name="description" style="outline: none !important;border-color: #719ECE;box-shadow: 0 0 10px #719ECE;">{!! $product->description !!} </textarea>
                            </div>
                            <div class="form-group">
                                <label class="form-label">Condition</label>
                                <textarea class="form-control" id="condition" rows="5" name="condition" style="outline: none !important;border-color: #719ECE;box-shadow: 0 0 10px #719ECE;"> {!! $product->condition !!}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Categories
                            </h2>
                        </div>
                        <div class="body">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="category">Categories</label>
                                    <select name="category" id="category" class="form-control show-tick" data-live-search="true">
                                        @foreach($categories as $category)
                                            <option  {{ $product->category_id == $category->id ? 'selected': ''}}
                                                     value="{{ $category->id }}">
                                                {{ $category->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- Vertical Layout | With Floating Label -->
    </div>

@endsection

@push('js')
    <!-- Select Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
@endpush
